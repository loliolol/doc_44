package src.Project;

import java.io.File;
//import java.io.FileReader;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import java.io.FileReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;


public class Main {
    private static final String FILE_TEST = "C:\\rjl.java";


    public static void main(String[] args) throws IOException {
        ArrayList<String> list = new ArrayList<>();
        try {
            final Scanner test = new Scanner((new File(FILE_TEST)));
            ArrayList data_array = new ArrayList();
            while (test.hasNext()) {
                String data = test.nextLine();
                data_array.add(data);
            }
            test.close();

            //data_array.forEach(System.out::println);

            for (int i = 0; i < data_array.size(); i++) {

                if (data_array.get(i).toString().contains("/**") && data_array.get(i + 1).toString().contains("* @class ")) {
                    int j = i + 1;
                    String line = "";
                    while (!data_array.get(j).toString().contains("*/")) {
                        // System.out.println(data_array.get(j).toString());
                        if (data_array.get(j).toString().contains("@class")) {
                            line += data_array.get(j).toString() + "/";
                        }
                        if (data_array.get(j).toString().contains("properties")) {
                            line += data_array.get(j).toString() + "/";
                        }
                        if (data_array.get(j).toString().contains("author")) {
                            line += data_array.get(j).toString() + "/";
                        }
                        if (data_array.get(j).toString().contains("version")) {
                            line += data_array.get(j).toString() + "/";
                        }
                        if (data_array.get(j).toString().contains("since")) {
                            line += data_array.get(j).toString();
                        }

                        j++;
                    }


                   //System.out.println(line);
                    list.add(line);


                }
                if (data_array.get(i).toString().contains("/**") && data_array.get(i + 2).toString().contains("Method")) {
                    int k = i + 1;
                    String line_methods = "";
                    while (!data_array.get(k).toString().contains("*/")) {
                        if (data_array.get(k).toString().contains("@method")) {
                            line_methods += data_array.get(k).toString() + "/";
                        }
                        if (data_array.get(k).toString().contains("Method")) {
                            line_methods += data_array.get(k).toString() + "/";
                        }
                        if (data_array.get(k).toString().contains("@param")) {
                            line_methods += data_array.get(k).toString() + "/";
                        }
                        if (data_array.get(k).toString().contains("@return")) {
                            line_methods += data_array.get(k).toString();
                        }
                        k++;
                    }
                  //  System.out.println(line_methods);
                    list.add(line_methods);
                }
            }


            } catch(FileNotFoundException e){
                e.printStackTrace();
            }


            String report = generateReport(list);
            saveToFile(report, "C:\\report.html");

        }


        private static String generateReport (ArrayList < String > list) {
            StringBuilder stringBuilder = new StringBuilder()
                    .append("<!DOCTYPE html>\n")
                    .append("<html lang=\"en\">\n")
                    .append("<head>\n")
                    .append("<meta charset=\"UTF-8\">\n")
                    .append(" <link rel=\"stylesheet\" href=\"style.css\"/>")
                   // .append("<link rel=\"stylesheet\" type=\"text/css\" href=\"stylesheet.css\" title=\"Style\">\n")
                    .append("<title>Хорошилова</title>\n")
                    .append("<style>\n")
                    .append("body {\n")
                    .append("background-color: #fff;\n")
                    .append("color: #353833;\n")
                    .append("font-family: Arial, sans-serif;\n")
                    .append("font-size: 14px;\n")
                    .append("}\n")
                    .append("table{\n")
                    .append("border:1px solid black;\n")
                    .append("margin: 1px;\n")
                    .append("}\n")
                    .append("table td{text-align: center;}\n")
                    .append("\n")
                    .append("</style>")
                    .append("</head>\n")
                    .append("<body>\n")
                    .append("<h1>Отчет JavaDoc</h1>\n");

            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).contains("* @class")) {
                    String clas [] ;
                    clas = list.get(i).split("/");
                    stringBuilder.append("<div>");

                    stringBuilder.append("<table width=\"100%\">")
                            .append("<tr>")
                            .append("<th>Class</th>")
                            .append("<th>Description</th>")
                            .append("<th>Author</th>")
                            .append("<th>Version</th>")
                            .append("<th>Since</th>")
                            .append("</tr>")
                            .append("<tr>");
                    for (int j=0; j<5; j++) {
                        String class_new [];
                        class_new = clas[j].split(" ");
                        String class_final = "";
                            for (int k=3; k<class_new.length; k++){
                                class_final+=class_new[k] + " ";
                            }
                        stringBuilder.append("<td>" + class_final + "</td>\n");
                    }
                    stringBuilder.append("</tr>")
                            .append("</table>")
                            .append("<br/>")
                            .append("</div>\n");
                }

                if (list.get(i).contains("* @method")) {
                    String clas [] ;
                    clas = list.get(i).split("/");
                    stringBuilder.append("<div>");

                    stringBuilder.append("<table width=\"100%\">")
                            .append("<tr>")
                            .append("<th>Signature</th>")
                            .append("<th>Description</th>")
                            .append("<th>Param</th>")
                            .append("<th>Return</th>")
                            .append("</tr>")
                            .append("<tr>");
                    for (int j=0; j<clas.length; j++) {
                        String class_middle = clas[j].replace("     ", "");
                        String class_new [];
                        class_new = class_middle.split(" ");
                        String class_final = "";
                        for (int k=2; k<class_new.length; k++){

                            class_final+= class_new[k] + " ";
                        }
                        stringBuilder.append("<td>" + class_final + "</td>\n");
                    }
                    stringBuilder.append("</tr>")
                            .append("</table>")
                            .append("<br/>")
                            .append("</div>\n");
                }



            }

            stringBuilder
                    .append("</body>\n")
                    .append("</html>");

            return stringBuilder.toString();

        }


        private static void saveToFile (String report, String fileName) throws IOException {
            OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(fileName));
            writer.write(report);
            writer.close();
        }

    }

